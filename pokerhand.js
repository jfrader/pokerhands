const express = require("express");
const { rankBoard, rankDescription } = require("phe");

const NUMBERS = [2, 3, 4, 5, 6, 7, 8, 9, "T", "J", "Q", "K", "A"];
const SOOTS = ["c", "s", "d", "h"];

const mix = (cards, times = Math.floor(Math.random() * 20)) => {
  let currentIndex = cards.length;
  let temp;
  let randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = cards[currentIndex];
    cards[currentIndex] = cards[randomIndex];
    cards[randomIndex] = temporaryValue;
  }

  if (times > 0) {
    return mix(cards, times - 1);
  }

  return cards;
};

const CARDS = mix([
  ...NUMBERS.reduce(
    (cardsAcc, cn) => [...cardsAcc, ...SOOTS.map(s => [cn, s])],
    []
  )
]);

(() => {
  const getDeck = () => {
    return [...CARDS];
  };

  const getHand = (deck, size = 7) => {
    return deck.splice(2 * 6 + 3, size);
  };

  const handToHTML = hand => {
    let html = "";
    hand.forEach(card => {
      html += `<img style="width: 100px;" src="/media/${card
        .join("")
        .toUpperCase()}.png">`;
    });
    return html;
  };

  const getRankString = hand => hand.map(c => c.join("")).join(" ")

  const getRank = hand => {
    return rankDescription[rankBoard(getRankString(hand))];
  };

  const getRankObj = hand => {
    return {
      text: getRank(hand),
      ranking: rankBoard(hand)
    }
  }

  const printHand = hand => {
    return `<div>${handToHTML(hand)}</div><h1>${getRank(hand)}</h1>`;
  };

  const app = express();

  app.use("/media", express.static("./media"));

  app.use((req, res, next) => {
    req.deck = mix(getDeck());
    next();
  });

  app.get("/random/:size?", (req, res, next) => {
    const size = req.params.size || 7;
    const hand = getHand(req.deck, size);
    res.send(`<body>${printHand(hand)}</body>`);
  });

  app.get("/holdem/:players?", (req, res, next) => {
    const players = parseInt(req.params.players) || 2;
    const hands = [];
    for(let i = 0; i < players ; i++) {
      hands.push(getHand(req.deck, 2))
    }
    const flop = getHand(req.deck, 5);

    const handsWithRank = hands.map(h => ({
      cards: h,
      rank: getRank(h.concat([...flop]))
    }));

    const handsHTML = handsWithRank.map((h, i) => `<h4>Player ${i + 1} -> ${h.rank}</h4><div>${handToHTML(h.cards)}</div><hr>`);

    res.send(`
      <body>
          <h1>Flop</h1>
          <div>
            ${handToHTML(flop)}
          </div><hr>
          ${handsHTML}
      </body>
    `);
  });

  app.listen(1010);
})();
